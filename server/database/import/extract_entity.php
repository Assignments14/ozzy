<?php

const SOURCE_FILE = 'prods_export.json';
const OUTPUT_DIR = 'entity/';

function clean_str_ctrl(string $s): string {
    return preg_replace('/[^\PC\s]/u', '', $s); 
}

function json_decode_err(string $json): array {
    return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
}

function array_dot_get(array $arr, string $key, $default = null) {
    if (array_key_exists($key, $arr)) {
        return $arr[$key];
    }

    if (strpos($key, '.') === false) {
        return $arr[$key] ?? $default;
    }

    foreach(explode('.', $key) as $segment) {
        if (is_array($arr) && array_key_exists($segment, $arr)) {
            $arr = $arr[$segment];
        } else {
            return $default;
        }
    }

    return $arr;
}

function output_file(string $entity): string {
    return OUTPUT_DIR . "$entity.json";
}

function clean_file(string $fn): void {
    file_put_contents($fn, '');
}

function eln(string $mes): void {
    echo $mes . PHP_EOL;
}

const MONGOIMPORT_TPL = 'mongoimport --db ozzyshop --collection %s --drop --file %s';

const SHOP_PREFIX = 'yml_catalog.shop.';

eln('Loading catalog...');
$catalog = json_decode_err(
    clean_str_ctrl(
        file_get_contents(SOURCE_FILE)
    )
);

$entity = [
    'currency' => 'currencies.currency',
    'category' => 'categories.category',
    'offer' => 'offers.offer',
];

foreach ($entity as $e_name => $e_path) {
    eln('Processing entity: ' . $e_name);
    $output_file = output_file($e_name);
    clean_file($output_file);
    $data = array_dot_get($catalog, SHOP_PREFIX . $e_path);
    foreach($data as $item) {
        $json_line = json_encode($item) . PHP_EOL;
        file_put_contents($output_file, $json_line, FILE_APPEND);
    }
    eln(sprintf(MONGOIMPORT_TPL, $e_name, $output_file));
    eln('DONE.');
}

