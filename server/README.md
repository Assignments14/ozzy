#### Prepare data

- [x] convert xml to json for mongoimport (oxygen)
- [x] extract entities for mongoimport (php script)
- [x] mongoimport
