<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;

class OfferByCategory extends Controller
{
    public function __invoke(Request $request, string $categoryId)
    {
        return Offer::byCategory($categoryId);
    }
}
