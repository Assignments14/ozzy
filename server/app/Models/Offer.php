<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Offer extends MongoModel
{
    protected $connection = 'mongodb';

    protected $collection = 'offer';

    protected $fillable = [];

    public function scopeByCategory($query, string $categoryId)
    {
        // 50447642
        return $query->where('categoryId', $categoryId)->get();
    }
}
