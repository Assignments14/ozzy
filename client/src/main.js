import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import './main.css'

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

const app = createApp(App)

app.axios = axios;
app.$http = axios;
app.config.globalProperties.axios = axios;
app.config.globalProperties.$http = axios;

app.mount('#app')
